// ==UserScript==
// @name         hima_04
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        http*://himachat.jp/*
// @require      
// @grant        none
// ==/UserScript==



class Himachat{
	constructor(){
		this.switchDisplay();
		this.createUIroom();
	}
	
	// ---------------------------------------------------------------
	// 
	switchDisplay(){
		// toppage
		const topStyleDisplay = document.querySelector('#top').style.display;
		if(topStyleDisplay === 'block' | !topStyleDisplay){
			console.log('top');
		}
		// room
		else if(document.querySelector('#roompage').style.display === 'block'){
			console.log('room');
			if(document.querySelector('#roompage').children[0].id.indexOf('u') !== -1){
				console.log('kocha');
			}
		}
	
	}
	
	// ---------------------------------------------------------------
	// 部屋用UI
	createUIroom(){
		this.btnCreateImgLink();
		this.aGetRoomLog();
		
		this.btnGetMembers();
		this.aGetMembers();
		
		// 「このページから移動しますか？」の確認メッセージを表示しない
		window.onbeforeunload = function(event){}
	}
	
	// ---------------------------------------------------------------
	// createImgLink
	
	btnCreateImgLink(){
		var ks = document.querySelector('#keikokuspace');
		var btn = document.createElement('button');
		btn.appendChild(document.createTextNode('createImgLink'));
		ks.appendChild(btn);
		
		btn.addEventListener('click', this.btnCreateImgLink_click.bind(this));
	}
	btnCreateImgLink_click(){
		var chatTables = document.querySelectorAll('.chatwindow table');
		for(var ct of chatTables){
			// コメント
			if(ct.className === 'coment'){
				// 画像
				if(ct.querySelector('.mozi').id.indexOf('photo') >= 0){
					var a = ct.querySelector('a');
					if(a.innerText.indexOf('画像を表示する') !== -1){
						var path = (a.onclick + '').split('"')[1];			// onclickのJSから画像URLを取り出す
						var url = 'https://'+ location.host +'/' + path;	// スキーマ違いやドメイン名が複数ある？
						//console.log(url);
						var link = document.createElement('a');
						link.href = url;
						link.target = '_blank';
						link.rel = 'noopener';
						link.appendChild(document.createTextNode('●'));
						ct.querySelector('.mozi').appendChild(link);
					}
				}
			}
		}
	}
	
	// ---------------------------------------------------------------
	// getMembers
	
	btnGetMembers(){
		var btn = document.createElement('button');
		btn.id = 'btnGetMembers';
		btn.appendChild(document.createTextNode('getMembers'));
		var ks = document.querySelector('#keikokuspace');
		ks.appendChild(btn);
		
		btn.addEventListener('click', this.btnGetMembers_click.bind(this));
	}
	btnGetMembers_click(e){
		// 最初の1回だけ実行。準備処理
		if(!document.querySelector('#taMemo')){
			// メモ用テキストエリアが存在しない場合、作成する
			var roomurlspace = document.querySelector(".roomurlspace");
			var ta = document.createElement('textarea');
			ta.id = 'taMemo';
			roomurlspace.appendChild(ta);
			
			// alert関数を置き換えて、ユーザーの「詳細」を押した時にテキストエリアに情報が登録されるようにする
			window.alert = function(msg){
				console.log(msg);
				document.querySelector('#taMemo').value += msg + '\n';
			}
		}
		
		// 太字/細字 で取得状況を表示する
		var a = document.querySelector('#aGetMembers');
		a.style.fontWeight = 'normal';
		
		// 全入室者の詳細を取得
		document.querySelector('#taMemo').value = '';
		var memberinfos = document.querySelectorAll('.members .memberinfo');
		for(var memberinfo of memberinfos){
			getMemberShousai(memberinfo);
		}
		function getMemberShousai(memberinfo){
			var span0 = memberinfo.querySelectorAll('span')[0];
			span0.click();
			var span1 = memberinfo.querySelectorAll('span')[1];
			span1.click();
		}
		setTimeout(()=>{
			// 部屋名と部屋ID取得
			var roomName = document.querySelector('.h1roomname').innerText;
			var roomUrl = document.querySelector('.roomurlspace input').value;
			var roomId = roomUrl
				.replace('http://himachat.jp?room=', '')
				.replace('https://himachat.jp?room=', '');
			
			// 現在日時取得
			var dt = new CDate();
			var strDateTime = dt.getDate() + "_" + dt.getTime();
			
			// 部屋リストを保存する
			var data = document.querySelector('#taMemo').value;
			var blob = new Blob([ data ], { "type" : "text/plain" });
			var a = document.querySelector('#aGetMembers');
			//console.log(a);
			a.download = '#hima@members#room_' + strDateTime +`_${roomId}_${roomName}.txt`;
			a.href = window.URL.createObjectURL(blob);
			a.style.fontWeight = 'bold';
			
			console.log('geted members');
		}, 3000);
	}
	aGetMembers(){
		var a = document.createElement('a');
		a.id = 'aGetMembers';
		a.appendChild(document.createTextNode('getMembers'));
		var ks = document.querySelector('#keikokuspace');
		ks.appendChild(a);
		
		a.addEventListener('click', this.aGetMembers_click.bind(this));
	}
	aGetMembers_click(){
		
	}
	
	// ---------------------------------------------------------------
	// getRoomLog
	
	aGetRoomLog(){
		var a = document.createElement('a');
		a.id = 'aGetChatLog';
		a.appendChild(document.createTextNode('getRoomLog'));
		var ks = document.querySelector('#keikokuspace');
		ks.appendChild(a);
		
		a.addEventListener('click', this.aGetRoomLog_click.bind(this));
	}
	aGetRoomLog_click(){
		console.log(111);
		var log = '';
		var chatTables = document.querySelectorAll('.chatwindow table');
		for(var ct of chatTables){
			// コメント
			if(ct.className === 'coment'){
				var name = ct.querySelector('.name').innerText;
				var nameColor = ct.querySelector('.name font').color;
				
				// 画像
				if(ct.querySelector('.mozi').id.indexOf('photo') >= 0){
					//var photoNo = ct.querySelector('.mozi').id;
					//console.log('compho', time, ip, name, nameColor, photoNo);
					//var logLine = `compho\t${time}\t${ip}\t${name}\t${nameColor}\t${photoNo}`;
					var a = ct.querySelector('a');
					if(a.innerText.indexOf('画像を表示する') !== -1){
						var path = (a.onclick + '').split('"')[1];
					}
					else{
						// 画像を表示した後はここに来る。しかし、時間やIPが消えてるので非サポートとする。
						//a = ct.querySelector('.sw2 a');
					}
					var path = (a.onclick + '').split('"')[1];	// onclickのJSから画像URLを取り出す
					var url = 'http://'+ location.host +'/' + path;
					var logLine = `compho\t${time}\t${nameColor}\t${ip}\t${name}\t${url}`;
				}
				// 発言
				else{
					var smallFontText = ct.querySelector('small font').innerText;
					var smallFont = smallFontText.split(' ');
					var time = smallFont[0];
					var ip = smallFont[1];
					
					var talkText = ct.querySelector('.allblack').innerText;
					console.log('coment', time, ip, name, nameColor, talkText);
					var logLine = `coment\t${time}\t${nameColor}\t${ip}\t${name}\t${talkText}`;
				}
			}
			// システムメッセージ
			else{
				var ctText = ct.innerText;
				if(ctText.indexOf('入室しました') !== -1){
					var texts = ctText.split(' ');
					//var text = texts[0].replace('さんが入室しました', '');	// ★★★　lastIndexOfに変える
					var last = ctText.lastIndexOf('さんが入室しました');
					var name = ctText.substr(0, last);
					var date = texts[1];
					time = texts[2];
					//console.log('system', time, '-.-.-.-', name, '----', '入室');
					var logLine = `system\t${time}\t\t\t${name}\t入室`;
				}
				else if(ctText.indexOf('退出しました') !== -1){
					console.log('system', ctText);
					var last = ctText.lastIndexOf('さんは退出しました');
					var name = ctText.substr(0, last);
					var logLine = `system\t\t\t\t${name}\t退出`;
				}
				// 「oldnameさんは、newnameさんになったそうです」
				// 簡易パースなので、「oldnameさんは、さんは、newnameさんになったそうです」などはおかしくなる
				else if(ctText.indexOf('になったそうです') !== -1){
					var texts = ctText.split('さんは、');
					var oldname = texts[0];
					var last = texts[1].lastIndexOf('さんになったそうです');
					var newname = texts[1].substr(0, last);
					//console.log('system', '--:--:--', '-.-.-.-', oldname, '名前変更', newname);
					var logLine = `system\t\t\t${oldname}\t名前変更\t${newname}`;
				}
				// nameさんが画像photoNoを削除しました
				else if(ctText.indexOf('を削除しました') !== -1){
					var photoNoEnd = ctText.lastIndexOf('を削除しました');
					var photoNoStart = 2 + ctText.lastIndexOf('画像');
					var nameEnd = photoNoStart - 5;
					var name = ctText.substring(0, nameEnd);
					var photoNo = ctText.substring(photoNoStart, photoNoEnd);
					console.log(name, photoNo, '!!!!!!!!!!!!');
				}
				else{
					console.log('!!!!! システムメッセージの解析エラー !!!!!!!!!');
				}
			}
			log += logLine + '\n';
		}
		
		// 部屋名と部屋ID取得
		var roomName = document.querySelector('.h1roomname').innerText;
		var roomUrl = document.querySelector('.roomurlspace input').value;
		var roomId = roomUrl
			.replace('http://himachat.jp?room=', '')
			.replace('https://himachat.jp?room=', '');
		
		// 現在日時取得
		var dt = new CDate();
		var strDateTime = dt.getDate() + "_" + dt.getTime();
		
		// 部屋リストを保存する
		var data = log;
		var blob = new Blob([ data ], { "type" : "text/plain" });
		var a = document.querySelector('#aGetChatLog');
		a.download = '#hima#room_' + strDateTime +`_${roomId}_${roomName}.txt`;
		a.href = window.URL.createObjectURL(blob);
	}
}

class CDate{
	getDate(){
		var t = new Date();
		var y = t.getFullYear();
		var m = ( '00' + (t.getMonth() + 1) ).slice(-2)
		var d = ( '00' + t.getDate() ).slice(-2);
		//console.log(y, m, d);
		var dateString = y+'-'+m+'-'+d;	// yyyy-dd-mm
		
		return dateString;	
	}
	getTime(){
		var t = new Date();
		var h = ('00' + t.getHours() ).slice(-2);
		var m = ('00' + t.getMinutes() ).slice(-2);;
		var s = ('00' + t.getSeconds() ).slice(-2);
		//console.log(h+':'+m+':'+s);
		var timeString = h+m+s;	// hh:mm:ss
		
		return timeString;
	}
}

const hc = new Himachat();

